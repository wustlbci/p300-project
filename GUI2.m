function varargout = GUI2(varargin)
% GUI2 MATLAB code for GUI2.fig
%      GUI2, by itself, creates a new GUI2 or raises the existing
%      singleton*.
%
%      H = GUI2 returns the handle to a new GUI2 or the handle to
%      the existing singleton*.
%
%      GUI2('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI2.M with the given input arguments.
%
%      GUI2('Property','Value',...) creates a new GUI2 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before GUI2_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to GUI2_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help GUI2

% Last Modified by GUIDE v2.5 17-Sep-2013 14:31:08

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @GUI2_OpeningFcn, ...
                   'gui_OutputFcn',  @GUI2_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before GUI2 is made visible.
function GUI2_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to GUI2 (see VARARGIN)

% Choose default command line output for GUI2
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes GUI2 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = GUI2_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in Run_Button.
function Run_Button_Callback(hObject, eventdata, handles)
% hObject    handle to Run_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%path = get(handles.File_Path, 'String');
path = get(handles.File_Path, 'String');
disp(path)
channelNum = str2num(get(handles.num_electrode, 'String'));
typeNum = str2num(get(handles.num_types, 'String'));
oddballTrials = str2num(get(handles.OddballTrials, 'String'));

% Load file
[ signal, states, parameters ] = load_bcidat(path);
signal = double(signal);

% Parameter extraction
sampleRate = parameters.SamplingRate.NumericValue;
stimuli = states.StimulusCode;
numChannels = size(signal,2);

% Perform a CAR filter
% This might be bad for ERPs...
for ch=1:size(signal, 2)
    signal(:, ch)=signal(:, ch)-mean(signal(:, ch));
end
signal = carFilt(signal,2);

% Group data by stimulus code (new method)
for stimToChunk = 1:typeNum
    stimEpoch = find(stimuli==stimToChunk);
    % stimulus begin index
    stimBeginInd = [1; stimEpoch(find(diff(double(stimEpoch))~=1) + 1)];
    % Remove 1st chunk
    if stimuli(1)==stimToChunk
        % Sometimes, there is an incorrectly recorded, short burst of stimuli 1 at the
        % beginning of the data set for no reason. Remove that.
        stimBeginInd = stimBeginInd(2:end);
    end
    % %
    Trials = zeros(128,numChannels,length(stimBeginInd));
    epochLength = 128; % number of samples
    % %
    for stimChunkNo = 1:length(stimBeginInd)
        signalTrial = signal(stimBeginInd(stimChunkNo): stimBeginInd(stimChunkNo)+epochLength-1, :);     
            Trials(:,:,stimChunkNo) = signalTrial;
    end
    
    eval(['Trials' num2str(stimToChunk) '=' 'Trials;']);
end

% Wavelet Analysis 
timeVector = linspace(0,1000,epochLength); % seconds

for stimToChunk = 1:typeNum % for CWr01s03, stim1 is special
    eval(['nTrials' '=' 'Trials' num2str(stimToChunk) ';']) % Dummy matrix, data in
     %sTrials = zeros(32,14,size(nTrials,3)); % Dummy matrix, data out
     wTrials = zeros(10,128,size(nTrials,3));
    for stimChunkNo = 1:size(nTrials,3)
        for channel = channelNum
            analyzeThis = nTrials(:,channel,stimChunkNo);
            % Continuous wavelet transform
            ccfs=cwt(analyzeThis,1:10,'mexh'); % complex
%             subplot(2,1,1) % time domain
%             plot(timeVector, analyzeThis)
%             xlabel('time (ms)'); ylabel('uV')
%             title(['Raw data, Ch' num2str(channel)])
%             subplot(2,1,2) % wavelet transform
%             imagesc(timeVector,1:10,abs(ccfs));
%             colormap jet; axis xy;
%             xlabel('time (ms)'); ylabel('Scales');
%             title('Wavelet Transform')
            
           %pause
           wTrials(:,:,stimChunkNo) = abs(ccfs);
           eval(['wTrials' num2str(stimToChunk) ' = wTrials;']);
        end
    end
end

%
%R-squared for wavelet analysis

% OfflineAnalysisSpectral script
% Make sure BCI2000\Tools /s are in the Matlab path


epochLength = 128; % number of samples
stimuli = states.StimulusCode;


TargetTrials = [];
NoTargetTrials = [];
for types = 1:typeNum
eval(['WTrials' '=' 'wTrials' num2str(types) ';'])
% StimulusCodeChangeIndices
stimToChunk = types;
stimEpoch = find(stimuli==stimToChunk);
% stimulus begin index
stimBeginInd = [1; stimEpoch(find(diff(double(stimEpoch))~= 1) + 1)];
% Remove 1st chunk
if stimuli(1)==stimToChunk
    % Sometimes, there is an incorrectly recorded, short burst of stimuli 1 at the
    % beginning of the data set for no reason. Remove that.
    stimBeginInd = stimBeginInd(2:end);
end

tempMatrix = zeros(epochLength, 10, 40); %time vs. scales vs. cwt values
for k = 1:length(stimBeginInd)
   data = WTrials(:,:,k);
   tempMatrix(:,:,k) = data'; 
end

eval(['RTrials' num2str(types) ' = tempMatrix;']);
%stimEpoch = [];

isOddball = ismember(types, oddballTrials); %asks whether the trial is an oddball trial
if isOddball == 1
    TargetTrials = cat(3, TargetTrials, tempMatrix);
else
    NoTargetTrials = cat(3, NoTargetTrials, tempMatrix);
end

tempMatrix = [];
stimBeginInd = [];
end

%%%%%***********************************

%NoTargetTrials = cat(3, RTrials1, RTrials2, RTrials3, RTrials4, RTrials6, RTrials7, RTrials8);
%TargetTrials = cat(3,RTrials9,RTrials10);


R_Squared = calc_rsqu(NoTargetTrials,TargetTrials, 1);
timeVector = (1:epochLength)/128*1000;
%channelVector = 1:14;
%figure(1),surf(timeVector, channelVector, R_Squared3','EdgeColor','none'),view(2),colorbar;
axes(handles.axes1),imagesc(timeVector, 1:10, R_Squared'), colorbar
set(gcf,'Renderer','Zbuffer');
title('R^2 Plot'),xlabel('Time(ms)'),ylabel('Scale')



function File_Path_Callback(hObject, eventdata, handles)
% hObject    handle to File_Path (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of File_Path as text
%        str2double(get(hObject,'String')) returns contents of File_Path as a double
file = get(File_Path, 'String');
file



% --- Executes during object creation, after setting all properties.
function File_Path_CreateFcn(hObject, eventdata, handles)
% hObject    handle to File_Path (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function num_electrode_Callback(hObject, eventdata, handles)
% hObject    handle to num_electrode (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of num_electrode as text
%        str2double(get(hObject,'String')) returns contents of num_electrode as a double
channel = str2num(get(hObject, 'String'));


% --- Executes during object creation, after setting all properties.
function num_electrode_CreateFcn(hObject, eventdata, handles)
% hObject    handle to num_electrode (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function num_types_Callback(hObject, eventdata, handles)
% hObject    handle to num_types (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of num_types as text
%        str2double(get(hObject,'String')) returns contents of num_types as a double
types = str2num(get(hObject, 'String'));



% --- Executes during object creation, after setting all properties.
function num_types_CreateFcn(hObject, eventdata, handles)
% hObject    handle to num_types (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function NullTrials_Callback(hObject, eventdata, handles)
% hObject    handle to NullTrials (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of NullTrials as text
%        str2double(get(hObject,'String')) returns contents of NullTrials as a double
null = str2num(get(hObject, 'String'));


% --- Executes during object creation, after setting all properties.
function NullTrials_CreateFcn(hObject, eventdata, handles)
% hObject    handle to NullTrials (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function OddballTrials_Callback(hObject, eventdata, handles)
% hObject    handle to OddballTrials (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of OddballTrials as text
%        str2double(get(hObject,'String')) returns contents of OddballTrials as a double
oddball = str2num(get(hObject, 'String'));


% --- Executes during object creation, after setting all properties.
function OddballTrials_CreateFcn(hObject, eventdata, handles)
% hObject    handle to OddballTrials (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ChooseFile.
function ChooseFile_Callback(hObject, eventdata, handles)
% hObject    handle to ChooseFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[FileName, PathName] = uigetfile('../../../BCI2000/data/P300Generation/*.dat', 'MultiSelect','on');

set(handles.File_Path, 'String', [PathName, FileName]);

% Find the session and run #s within the file name.
%S = max(strfind(FileName, 'S'));
%R = max(strfind(FileName, 'R'));
%ext = max(strfind(FileName, '.'));
%session = FileName(S+1 : R-1)
%run = FileName(R+1 : ext-1)

%set(handles.num_electrode, 'String', session);
%set(handles.num_types, 'String', run);
if(FileName == 0)
    return
end
