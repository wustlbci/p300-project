% 12/13/13
% Analyzing results of ICA
% Jason Dunkley and Jenny Liu
% After performing independent componenent analysis via EEGLab,the original
% EEG data and ICA weights can be used to calculate the components and 
% plot them.

%% Performing ICA with EEGLab: 
% From scratch 
% 1. Run eeglab
% 2. Import data "From BCI2000 .DAT File"
%   Assume the eloc file is already loaded
%   Note: if you are using the BCI laptop, the eloc file is pre-loaded
%   (don't worry about it)
% 3. Select AF3-AF4 (all numbered ones except FP1, FP2)
% 4. Tools --> "Run ICA"
% 5. Save workspace as a set file ("Save Current Dataset As")
%%   From existing set file
% 1. Load set file
% 2. If not already done, do 2 & 3 from above
%   Example set file
%   C:\Users\wustlbci\Dropbox\IpsiHand\EEGLab\sample_data\test_bcidata_002.set
%   Already has everything listed in "From scratch" step 2 loaded and is
%   all set to go!
%% Analysis
% Open 'top_ICAana.m' and change appropriate parameters according to the
% google doc
% Run 'top_ICAana.m'


%% Parameters for experiment:
clear; clc;
%Don't change these!
% EPOCHLENGTH = Number of samples after each stimulus presentation that are analyzed. 
% e.g. EPOCHLENGTH = 128 means we analyze 1 second (128 Hz sampling rate) after the image is presented
EPOCHLENGTH = 128; 
SCALENO = 10;
CHANNELNO = 7;

%Change these for every run depending on params file

% STIMNO = Number of stimuli
% e.g. if #'s 1-6 are presented, STIMNO = 6
STIMNO = 15;
TARGETMATRIX = [13:15];
NOTARGETMATRIX =[1:12];

%% Browse for BCI2000 data file to analyze
[file path] = uigetfile('.dat');
dataFilePath = strcat(path,file);
disp(dataFilePath);

% Load .dat file into MATLAB workspace
LoadData

%% Import .dat file into EEGLab and run ICA
icaResults = pop_loadBCI2000(dataFilePath);
data = icaResults.data;
[weights sphere] = runica(data);

%% Re-format ICA components to something useable by our code
% datacomp is matrix of components (ICA outputs)

dataComp = weights * data;
signal = dataComp';


for i=1:14
    CHANNELNO = i;
    %stim2chunk
    Stim2Chunk2
    %wavelet transform
    WaveTrans
    %R-Squared
    WaveRSQU
    %Plotting
    PlotRSQU
end

