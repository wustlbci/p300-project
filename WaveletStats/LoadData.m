%% Load file
%inputs needed: string dataFilePath
[ signal, states, parameters ] = load_bcidat(dataFilePath);
signal = double(signal);

% % Parameter extraction
sampleRate = parameters.SamplingRate.NumericValue;
stimuli = states.StimulusCode;
numChannels = size(signal,2);