%% Perform a CAR filter
% This might be bad for ERPs...

% input: matrix signal
for ch=1:size(signal, 2)
    signal(:, ch)=signal(:, ch)-mean(signal(:, ch));
end
signal = carFilt(signal,2);