%dataFilePath = 'C:\Users\wustlbci\Dropbox\IpsiHand\BCI2000\data\P300Generation\ChristineWestonS001R03';
[file path] = uigetfile('.dat');
dataFilePath = strcat(path,file);
disp(dataFilePath);
%%

%Don't change these!
EPOCHLENGTH = 128;
SCALENO = 10;
CHANNELNO = 7;

%Change these for every run depending on params file
STIMNO = 15;
TARGETMATRIX = [13,14,15];
NOTARGETMATRIX =[1:12];
%% 
%loading data
LoadData
%%
%signal processing
SigProc
%%

for i=1:14
    CHANNELNO = i;
    %stim2chunk
    Stim2Chunk2
    %wavelet transform
    WaveTrans
    %R-Squared
    WaveRSQU
    %Plotting
    PlotRSQU
end
