%R-squared for wavelet analysis
% inputs needed:
    % TARGETMATRIX = matrix of the stimuli numbers that were "oddballs"
    % NOTARGETMATRIX = matrix of the stimuli that are not oddballs

% for stimToChunk = 1:STIMNO
%     
% % StimulusCodeChangeIndices
% stimEpoch = find(stimuli==stimToChunk);
% % stimulus begin index
% stimBeginInd = [1; stimEpoch(find(diff(double(stimEpoch))~= 1) + 1)];
% % Remove 1st chunk
% if stimuli(1)==stimToChunk
%     % Sometimes, there is an incorrectly recorded, short burst of stimuli 1 at the
%     % beginning of the data set for no reason. Remove that.
%     stimBeginInd = stimBeginInd(2:end);
% end
% 
% electrodes = size(signal,2);
% tempMatrix = zeros(epochLength, electrodes, length(stimBeginInd)); 
% for k = 1:length(stimBeginInd)
%    data = signal((stimBeginInd(k):stimBeginInd(k)+epochLength-1),:);
%    tempMatrix(:,:,k) = data; 
% end
% 
% eval(['Trials' num2str(stimToChunk) ' = tempMatrix;']);
% tempMatrix = [];
% stimBeginInd = [];
% end

% Make 2 piles of target and non-target data
% % Figure out total 3rd dimension length needed for 2 piles
lengthTarget = 0;
lengthNull = 0;
for i= 1:STIMNO
    if ismember(i,TARGETMATRIX)
        eval(['lengthTarget = lengthTarget + size(wTrials' num2str(i) ',3);'])
    elseif ismember(i,NOTARGETMATRIX)
        eval(['lengthNull = lengthNull + size(wTrials' num2str(i) ',3);'])
    end
end
% % Make empty bins with exact sizes needed for 2 piles
NoTargetTrials = NaN(size(wTrials1,1), size(wTrials1,2), lengthNull);
TargetTrials = NaN(size(wTrials1,1), size(wTrials1,2), lengthTarget);

% % Fill the 2 piles of target and non-target data
targetCounter = 1;
nullCounter = 1; 
for i = 1:STIMNO
    eval(['trialSize = size(wTrials' num2str(i) ');'])
    if ismember(i,TARGETMATRIX)
        begin = num2str(targetCounter);
        stop = num2str(targetCounter + trialSize(3) - 1);
        eval(['TargetTrials(:,:,' begin ':' stop ')'...
            '= wTrials' num2str(i) ';'])
        targetCounter = targetCounter + trialSize(3); 
    elseif ismember(i,NOTARGETMATRIX)
        begin = num2str(nullCounter);
        stop = num2str(nullCounter + trialSize(3) - 1);
        eval(['NoTargetTrials(:,:,' begin ':' stop ')'...
            '= wTrials' num2str(i) ';'])
        nullCounter = nullCounter + trialSize(3); 
     end
end

% Calculate the r^2 (finally)
R_Squared3 = calc_rsqu(NoTargetTrials,TargetTrials, 1);
