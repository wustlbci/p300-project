% Stim 2 Chunk
% Group data by stimulus code (new method)
% inputs needed:
    % constant STIMNO = number of stimuli
        % e.g. if #'s 1-6 are presented, STIMNO = 6
    % constant EPOCHLENGTH = number of samples after each stimulus
        % presentation that are analyzed
        % e.g. EPOCHLENGTH = 128 means we analyze 1 second (128 Hz sampling
        % rate) after the image is presented
for stimToChunk = 1:STIMNO
    
    stimEpoch = find(stimuli==stimToChunk);
    % stimulus begin index
    stimBeginInd = [1; stimEpoch(find(diff(double(stimEpoch))~=1) + 1)];
    % Remove 1st chunk
    if stimuli(1)==stimToChunk
        % Sometimes, there is an incorrectly recorded, short burst of stimuli 1 at the
        % beginning of the data set for no reason. Remove that.
        stimBeginInd = stimBeginInd(2:end);
    end
    % %
    Trials = zeros(128,numChannels,length(stimBeginInd));
    epochLength = EPOCHLENGTH; % number of samples
    % %
    
    for stimChunkNo = 1:length(stimBeginInd) %building sheets
     
        signalTrial = signal(stimBeginInd(stimChunkNo): stimBeginInd(stimChunkNo)+epochLength-1, :);     
            Trials(:,:,stimChunkNo) = signalTrial;
    end
    
    eval(['Trials' num2str(stimToChunk) '=' 'Trials;']);
end
