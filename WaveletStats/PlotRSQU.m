% Plot! 
timeVector = (1:epochLength)/128*1000;
scaleVector = 1:SCALENO;
figure()
imagesc(timeVector, scaleVector ,R_Squared3), colorbar
set(gcf,'Renderer','Zbuffer');
title('R^2 Plot'),xlabel('Time(ms)'),ylabel('Scale')