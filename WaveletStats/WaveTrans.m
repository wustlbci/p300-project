%% Wavelet Analysis (7/28/13)
% inputs needed:
    % CHANNELNO = electrode number of interest
    % SCALENO = number of scales desired for wavelet transform

for stimToChunk = 1:STIMNO % for CWr01s03, stim1 is special
    eval(['nTrials' '=' 'Trials' num2str(stimToChunk) ';']) % Dummy matrix, data in
    wTrials = zeros(SCALENO, EPOCHLENGTH, size(nTrials,3)); % Dummy matrix, data out
%     sTrials = zeros(32,14,size(nTrials,3)); % Dummy matrix, data out KILL
    for stimChunkNo = 1:size(nTrials,3) % number of "sheets"
        for channel = CHANNELNO
            analyzeThis = nTrials(:,channel,stimChunkNo);
            % Continuous wavelet transform
            ccfs=cwt(analyzeThis,1:SCALENO,'mexh'); % complex
            wTrials(:,:,stimChunkNo) = abs(ccfs);
            eval(['wTrials' num2str(stimToChunk) ' = wTrials;']); % save it
            
%             % For visualization 
%             timeVector = (1:epochLength)/128*1000; % don't put this here
%             % if you are actually plotting. It will be slow. 
%             subplot(2,1,1) % time domain
%             plot(timeVector, analyzeThis)
%             xlabel('time (ms)'); ylabel('uV')
%             title(['Raw data, Ch' num2str(channel)])
%             subplot(2,1,2) % wavelet transform
%             imagesc(timeVector,1:32,abs(ccfs));
%             colormap jet; axis xy;
%             xlabel('time (ms)'); ylabel('Scales');
%             title('Wavelet Transform')
%             
           % pause
          
        end
    end
end

% Note:
    % meyr, gaus, mexh, coif (maybe too sharp)
    % no: mexh
    % maybe: meyr
    % coif2--maybe
    % pretty good: gaus (2-6)
            