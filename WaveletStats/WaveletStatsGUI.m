function varargout = WaveletStatsGUI(varargin)
% WAVELETSTATSGUI MATLAB code for WaveletStatsGUI.fig
%      WAVELETSTATSGUI, by itself, creates a new WAVELETSTATSGUI or raises the existing
%      singleton*.
%
%      H = WAVELETSTATSGUI returns the handle to a new WAVELETSTATSGUI or the handle to
%      the existing singleton*.
%
%      WAVELETSTATSGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in WAVELETSTATSGUI.M with the given input arguments.
%
%      WAVELETSTATSGUI('Property','Value',...) creates a new WAVELETSTATSGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before WaveletStatsGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to WaveletStatsGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help WaveletStatsGUI

% Last Modified by GUIDE v2.5 10-Nov-2013 15:56:13

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @WaveletStatsGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @WaveletStatsGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before WaveletStatsGUI is made visible.
function WaveletStatsGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to WaveletStatsGUI (see VARARGIN)

% Choose default command line output for WaveletStatsGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes WaveletStatsGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = WaveletStatsGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in runButton.
function runButton_Callback(hObject, eventdata, handles)
% hObject    handle to runButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
dataFilePath = get(handles.path, 'String');
disp(path)
CHANNELNOMAT = str2num(get(handles.Electrode, 'String'));
STIMNO = str2num(get(handles.stimNo, 'String'));
TARGETMATRIX = str2num(get(handles.Oddball, 'String'));

%Constants that can't be changed from the front panel
EPOCHLENGTH = 128;
SCALENO = 10;
NOTARGETMATRIX = zeros(STIMNO-length(TARGETMATRIX));
count = 1;
for i = 1:STIMNO
    if ismember(i,TARGETMATRIX) == 0
        NOTARGETMATRIX(count) = i;
        count = count + 1;
    end
end

for chanNo = 1:length(CHANNELNOMAT)
    CHANNELNO = CHANNELNOMAT(chanNo);
    %loading data
    LoadData
    %signal processing
    SigProc
    %stim2chunk
    Stim2Chunk2
    %wavelet transform
    WaveTrans
    %R-Squared
    WaveRSQU
    %Plotting
    PlotRSQU
    set(figure(chanNo), 'Position', [350+50*chanNo 150 600 500])
end




function Electrode_Callback(hObject, eventdata, handles)
% hObject    handle to Electrode (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Electrode as text
%        str2double(get(hObject,'String')) returns contents of Electrode as a double


% --- Executes during object creation, after setting all properties.
function Electrode_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Electrode (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function stimNo_Callback(hObject, eventdata, handles)
% hObject    handle to stimNo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of stimNo as text
%        str2double(get(hObject,'String')) returns contents of stimNo as a double


% --- Executes during object creation, after setting all properties.
function stimNo_CreateFcn(hObject, eventdata, handles)
% hObject    handle to stimNo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Oddball_Callback(hObject, eventdata, handles)
% hObject    handle to Oddball (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Oddball as text
%        str2double(get(hObject,'String')) returns contents of Oddball as a double


% --- Executes during object creation, after setting all properties.
function Oddball_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Oddball (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function path_Callback(hObject, eventdata, handles)
% hObject    handle to path (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of path as text
%        str2double(get(hObject,'String')) returns contents of path as a double


% --- Executes during object creation, after setting all properties.
function path_CreateFcn(hObject, eventdata, handles)
% hObject    handle to path (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[FileName, PathName] = uigetfile('../../../BCI2000/data/P300Generation/*.dat', 'MultiSelect','on');
set(handles.path, 'String', [PathName, FileName]);

