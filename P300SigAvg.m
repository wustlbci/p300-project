% sampleCode_accessData
% How to extract EEG data from dat file
%% clear settings and load bci data
clear all; clf;
% Enter the filepath here (need to change for your computer, and relevant data)
user = 'SamShields'; session = '001'; run = '06'; testing = 'P300';
datapath = ['C:\Users\Jenny\Documents\MATLAB\BCI\data\' user 'S' session 'R' run '.dat']; %JennyLiu001\JennyLiuS001R02.dat';

% Load file
% signal has the EEG recordings for all channels
% You can get a piece of data by:
% getData = signal( indexBegin:indexEnd, channelNo)
% e.g. o1 = signal(:,7); o2 = signal(:,8); p7 = signal(:,6); p8 = signal(:,9);
% states has the stimuli codes
[signal, states, parameters] = load_bcidat(datapath, '-calibrated');
% %
% % Data Extraction
sampleRate = parameters.SamplingRate.NumericValue;
stimuli = states.StimulusCode;

[m n] = size(signal);

% Sampling rate; determined by the headset
Fs = 128;

% Time vector, in sample points (this may be useful for plotting)
pts = 1:m;

% Time vector, in seconds ((this may be useful for plotting))
t = (1:m)/128;

% % Identify beginning of P300
stim2 = find(stimuli==2);
stimToChunk = stim2; %what are you looking for the edges?
% ChunkNo = 5; % how many stimuli of this type were there?
needRemoveFirstChunk = 0;
% end of things needed to change
STCLess1 = stimToChunk(1:end-1);
STCDiff = stimToChunk(2:end) - STCLess1;
STCRightEdgeIndex = [find(STCDiff ~= 1); length(stimToChunk)]; % indices of stimToChunk bracketing right of each chunk
STCLeftEdgeIndex = [1; STCRightEdgeIndex(1:(end-1)) + 1]; % indices of stimToChunk bracketing left of each chunk

% Plot to confirm
plot(stimuli, 'Color', 'r');
hold on
for i = 1:length(STCRightEdgeIndex)
    position = stimToChunk(STCRightEdgeIndex(i));
    line([position position],[0 5]);
end
for i = 1:length(STCLeftEdgeIndex)
    position = stimToChunk(STCLeftEdgeIndex(i));
    line([position position],[0 5], 'Color','m');
end

% Remove 1st chunk
if stimuli(1)~=0
    % Sometimes, there is an incorrectly recorded, short burst of stimuli 1 at the
    % beginning of the data set for no reason. Remove that.
    STCRightEdgeIndex = STCRightEdgeIndex(2:end); STCLeftEdgeIndex = STCLeftEdgeIndex(2:end);
end

%%
dataLength = 64; timeVector = (1:dataLength)/Fs*1000; % milliseconds

trials = length(STCLeftEdgeIndex);
rawData = zeros(trials, 14, dataLength);
averageData = zeros(14, dataLength);

showRawData = 1; % 1 to show, 0 to not show
for electrode = 1:14
    
    for i = 1:length(STCLeftEdgeIndex)
        startIndex = STCLeftEdgeIndex(i);
        position = stimToChunk(startIndex);
        rawData(i, electrode, :) = signal(position:(position+dataLength-1), electrode);
        if showRawData == 1
            % Figure of all the pieces of data...
            subplot(4,6,i)
            testData = squeeze(rawData(i, electrode, :));
            plot(timeVector, testData)
            hold on
            plot(300, rawData(i, electrode, 38),'o','Color','m')
            hold off
            xlim([0 500])
            xlabel('Time (ms)')
        end
        averageData(electrode, :) = averageData(electrode,:) + transpose(signal(position:(position+dataLength-1), electrode));
    end
    if showRawdata == 1
        pause
    end
end
%%
averageData = averageData/trials;

for channel = 1:14
    subplot(3,5,channel)
    plot(timeVector, averageData(channel,:))
    hold on
    plot(300, averageData(channel,38),'o','Color','m')
    hold off
    title([ 'Electrode ' num2str(channel)])
    xlim([0 500])
end