%

%% Load file
filepath = 'C:\BCI2000\data\samplefiles'; filename = 'eeg1_2.dat';
[ signal, states, parameters ] = load_bcidat([filepath '\' filename]);
signal = double(signal);

% % Parameter extraction
sampleRate = parameters.SamplingRate.NumericValue;
stimuli = states.StimulusCode;
numChannels = size(signal,2);
%% Perform a CAR filter
% This might be bad for ERPs...
for ch=1:size(signal, 2)
    signal(:, ch)=signal(:, ch)-mean(signal(:, ch));
end
signal = carFilt(signal,2);

% %% Group data by stimulus code (old method)
% stim2 = find(stimuli==2);
% stimToChunk = stim2; %for which stimulus do you seek the edges?
% % ChunkNo = 5; % how many stimuli of this type were there?
% needRemoveFirstChunk = 0;
% % end of things needed to change
% STCLess1 = stimToChunk(1:end-1);
% STCDiff = stimToChunk(2:end) - STCLess1;
% STCRightEdgeIndex = [find(STCDiff ~= 1); length(stimToChunk)]; % indices of stimToChunk bracketing right of each chunk
% STCLeftEdgeIndex = [1; STCRightEdgeIndex(1:(end-1)) + 1]; % indices of stimToChunk bracketing left of each chunk
%% Group data by stimulus code (new method)
for stimToChunk = 1:2
    stimEpoch = find(stimuli==stimToChunk);
    % stimulus begin index
    stimBeginInd = [1; find(diff(double(stimEpoch))~=1) + 1];
    % Remove 1st chunk
    if stimuli(1)==stimToChunk
        % Sometimes, there is an incorrectly recorded, short burst of stimuli 1 at the
        % beginning of the data set for no reason. Remove that.
        stimBeginInd = stimBeginInd(2:end);
    end
    
    Trials = zeros(128,numChannels,length(stimBeginInd));
    epochLength = 128; % number of samples
    % %
    for stimChunkNo = 1:length(stimBeginInd)
        signalTrial = signal(stimBeginInd(stimChunkNo): stimBeginInd(stimChunkNo)+epochLength-1, :);     
            Trials(:,:,stimChunkNo) = signalTrial;
    end
    
    eval(['Trials' num2str(stimToChunk) '=' 'Trials;']);
end
%% Group data by stimulus code
% StimulusCodeChangeIndices
% These are the indices where the stimulus code changes from one value
% to another. "diff" takes a derivative. We need to add some more to
% prevent allocating memory with each iteration.
SCCI = [1; find(diff(double(states.StimulusCode))~=0)+1; length(states.StimulusCode)];
SCCI = SCCI(1:30);


%% 

NullIndex = 1;
OddballIndex = 1;

for k = 1:length(SCCI)-1
    disp(k)
    % Signal analyzed in this particular epoch
    SignalTrial = signal((SCCI(k):SCCI(k)+timeIntervalSize-1),:);
    
    
    for electrode = 1:size(signal,2)
          PositiveSignalTrial = SignalTrial(:,electrode);
%         PositiveSignalTrial = SignalTrial(:,electrode).*SignalTrial(:,electrode);
          plot(PositiveSignalTrial); title(num2str(states.StimulusCode(SCCI(k)))); pause

        % Assign the average power to the corresponding stimulus
        if (states.StimulusCode(SCCI(k)) == 1)
            NullTrials(:,electrode,NullIndex) = PositiveSignalTrial;
            NullIndex = NullIndex+1;
            
            plot(NullTrials(:,electrode,NullIndex)); hold on
            plot(PositiveSignalTrial); hold off
            title([num2str(NullIndex) ', ' num2str(electrode)]); pause
           
        end
        
        if (states.StimulusCode(SCCI(k)) == 0)
            OddballTrials(:,electrode,OddballIndex) = PositiveSignalTrial;
            OddballIndex = OddballIndex+1;
            
            plot(OddballTrials(:,electrode,NullIndex)); hold on
            plot(PositiveSignalTrial); hold off
            title([num2str(OddballIndex) ', ' num2str(electrode)]); pause
        end
    end
end
%% 
% Calculated R^2
    % Requires calc_rsqu.m and rsqu.m
R_Squared = calc_rsqu(double(Trials1), double(Trials2),1);

% % Plot
timeVector = (1:timeIntervalSize)/128*1000;
channelVector = 1:64;
figure(1),surf(timeVector, channelVector, R_Squared','EdgeColor','none'),view(2),colorbar;
set(gcf,'Renderer','Zbuffer');

title('R^2 Plot'),xlabel('Time (ms)'),ylabel('Channel')