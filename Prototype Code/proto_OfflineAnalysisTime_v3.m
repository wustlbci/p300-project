% YJL
% Updated 7/14/2013



%% Load file
filepath = '\\warehouse.cec.wustl.edu\home\links\yjl1\My Documents\MATLAB\BCI2000\data'; filename = 'Christine07142013.dat';
[ signal, states, parameters ] = load_bcidat([filepath '\' filename]);
signal = double(signal);

% % Parameter extraction
sampleRate = parameters.SamplingRate.NumericValue;
stimuli = states.StimulusCode;
numChannels = size(signal,2);
%% Perform a CAR filter
% This might be bad for ERPs...
for ch=1:size(signal, 2)
    signal(:, ch)=signal(:, ch)-mean(signal(:, ch));
end
signal = carFilt(signal,2);
%% Group data by stimulus code (new method)
for stimToChunk = 1:6
    stimEpoch = find(stimuli==stimToChunk);
    % stimulus begin index
    stimBeginInd = [1; stimEpoch(find(diff(double(stimEpoch))~=1) + 1)];
    % Remove 1st chunk
    if stimuli(1)==stimToChunk
        % Sometimes, there is an incorrectly recorded, short burst of stimuli 1 at the
        % beginning of the data set for no reason. Remove that.
        stimBeginInd = stimBeginInd(2:end);
    end
    
    Trials = zeros(128,numChannels,length(stimBeginInd));
    epochLength = 128; % number of samples
    % %
    for stimChunkNo = 1:length(stimBeginInd)
        signalTrial = signal(stimBeginInd(stimChunkNo): stimBeginInd(stimChunkNo)+epochLength-1, :);     
            Trials(:,:,stimChunkNo) = signalTrial;
    end
    
    eval(['Trials' num2str(stimToChunk) '=' 'Trials;']);
end

%% Split into the trials with and without the target image
NullTrials = cat(3,Trials1,Trials2,Trials3,Trials4,Trials6);
OddballTrials = Trials5;

%% R^2 diagram
% Calculated R^2
    % Requires calc_rsqu.m and rsqu.m
R_Squared = calc_rsqu(double(Trials1), double(Trials2),1);

% % Plot
timeVector = (1:epochLength)/128*1000;
channelVector = 1:numChannels;
figure(1),surf(timeVector, channelVector, R_Squared','EdgeColor','none'),view(2),colorbar;
set(gcf,'Renderer','Zbuffer');

title('R^2 Plot'),xlabel('Time (ms)'),ylabel('Channel')