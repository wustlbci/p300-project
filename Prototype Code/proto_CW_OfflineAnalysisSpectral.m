% OfflineAnalysisSpectral script
% Make sure BCI2000\Tools /s are in the Matlab path

% Load .dat files
% [Filenames, BCIDatPath] = uigetfile( {'*.dat','BCI 2000 Mat Files (*.dat)'},'MultiSelect', 'on' ) ;
% Filenames = strcat(BCIDatPath,Filenames) ;
% files = struct( 'name', Filenames) ;
clear all
clc
[ signal, states, parameters ] = load_bcidat('\\warehouse.cec.wustl.edu\home\links\jak9\My Documents\MATLAB\BCI\SamShieldsS001R06.dat');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%signal: NxM array of samples from the electrodes during the experiment. N
%is the number of samples and M is the number of electrodes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%parameters.SamplingRate.NumericValue: Sample rate for data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%states.StimulusBegin: Nx1 column vector where N is the number of samples.
%This array alternates between 0 and 1 and changes everytime 
%states.StimulusCode changes indicating a new condition has begun.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%states.StimulusCode: Nx1 column vector where N is the number of samples.
%This array indicates the condition number for every sample.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%For example, in data\samplefiles\eeg1_1.dat the subjects starts at rest 
%(condition=0) and then raises both feet (condition2) at sample 673 and
%then goes back to rest at sample 1328. Therefore:
%states.SimulusBegin(1  :672 )=0 states.SimulusCode(1  :672 )=0   Rest
%states.SimulusBegin(673:1328)=1 states.SimulusCode(673:1328)=2   Both Feet
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Power Spectrum Constants
FSampling=parameters.SamplingRate.NumericValue; %sampling rate

for ch=1:size(signal, 2) %subtracts the offset
    signal(:, ch)=signal(:, ch)-mean(signal(:, ch));
end

signal = carFilt(signal,2); %filters the signal

% StimulusCodeChangeIndices
SCCI = [1; find(diff(double(states.StimulusCode))~=0)+1; length(states.StimulusCode)];

RestIndex = 1;
RedIndex = 1;
GreenIndex = 1;

samples = FSampling*2;
% RestTrials = zeros(TIME,size(signal,2),)

for k = 1:length(SCCI)-5
   data = signal((SCCI(k):SCCI(k)+samples),:);
   
   for j = 1:size(signal,2)
%        S = spectrogram(SignalTrial(:,j),80,40,80);   %not needed
%        PowerSpectrum = S.*conj(S);
%        AveragePower = mean(PowerSpectrum,2);
       
%        if (states.StimulusCode(SCCI(k)) == 0)
%            RestTrials(:,j,RestIndex) = data(:,j) ;
%        end
%        
       if (states.StimulusCode(SCCI(k)) == 1)
           RedTrials(:,j,RedIndex) = data(:,j) ;
       end
       
       if (states.StimulusCode(SCCI(k)) == 2)
           GreenTrials(:,j,GreenIndex) = data(:,j) ;
       end
       
   end
   
   if (states.StimulusCode(SCCI(k)) == 0)
       RestIndex = RestIndex +1 ;
   end
   
   if (states.StimulusCode(SCCI(k)) == 1)
       RedIndex = RedIndex +1 ;
   end
   
   if (states.StimulusCode(SCCI(k)) == 2)
       GreenIndex = GreenIndex +1 ;
   end
   
end
% 
% R_Squared = calc_rsqu(RestTrials,GreenTrials,1);
% R_Squared2 = calc_rsqu(RestTrials,RedTrials,1);
R_Squared3 = calc_rsqu(RedTrials,GreenTrials,1);


figure(1),surf(R_Squared3'),view(2),colorbar;
set(gcf,'Renderer','Zbuffer');
title('R^2 Plot'),xlabel('Time(ms)'),ylabel('Channel')

save('OfflineAnalysisSpectral');



