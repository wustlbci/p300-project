% YJL
% Updated 7/14/2013

% Moving average
% R^2 
%% Load file
% filepath = 'C:\Users\wustlbci\Desktop\P300 Stuff'; filename = 'ChristineWestonS002R03.dat';[ signal, states, parameters ] = load_bcidat([filepath '\' filename]);
signal = double(signal);
filepath = 'C:\Users\wustlbci\Dropbox\IpsiHand\BCI2000\data\P300Generation';
filename = 'ChristineWestonS001R03.dat';
% % Parameter extraction
sampleRate = parameters.SamplingRate.NumericValue;
stimuli = states.StimulusCode;
numChannels = size(signal,2);
%% Perform a CAR filter
% This might be bad for ERPs...
for ch=1:size(signal, 2)
    signal(:, ch)=signal(:, ch)-mean(signal(:, ch));
end
signal = carFilt(signal,2);
%% Group data by stimulus code (new method)
for stimToChunk = 1:10
    stimEpoch = find(stimuli==stimToChunk);
    % stimulus begin index
    stimBeginInd = [1; stimEpoch(find(diff(double(stimEpoch))~=1) + 1)];
    % Remove 1st chunk
    if stimuli(1)==stimToChunk
        % Sometimes, there is an incorrectly recorded, short burst of stimuli 1 at the
        % beginning of the data set for no reason. Remove that.
        stimBeginInd = stimBeginInd(2:end);
    end
    % %
    Trials = zeros(128,numChannels,length(stimBeginInd));
    epochLength = 128; % number of samples
    % %
    for stimChunkNo = 1:length(stimBeginInd)
        signalTrial = signal(stimBeginInd(stimChunkNo): stimBeginInd(stimChunkNo)+epochLength-1, :);     
            Trials(:,:,stimChunkNo) = signalTrial;
    end
    
    eval(['Trials' num2str(stimToChunk) '=' 'Trials;']);
end

%% Wavelet Analysis 
timeVector = linspace(0,1000,epochLength); % seconds

for stimToChunk = 1:10 % for CWr01s03, stim1 is special
    eval(['nTrials' '=' 'Trials' num2str(stimToChunk) ';']) % Dummy matrix, data in
     %sTrials = zeros(32,14,size(nTrials,3)); % Dummy matrix, data out
     wTrials = zeros(10,128,size(nTrials,3));
    for stimChunkNo = 1:size(nTrials,3)
        
        for channel = 11
            analyzeThis = nTrials(:,channel,stimChunkNo);
            % Continuous wavelet transform
            ccfs=cwt(analyzeThis,1:10,'mexh'); % complex
            % meyr, gaus, mexh, coif (maybe too sharp)
            % no: mexh
            % maybe: meyr
            % coif2--maybe
            % pretty good: gaus (2-6)
            % For visualization
            ['here', num2str(stimChunkNo)]
%             subplot(2,1,1) % time domain
%             plot(timeVector, analyzeThis)
%             xlabel('time (ms)'); ylabel('uV')
%             title(['Raw data, Ch' num2str(channel)])
%             subplot(2,1,2) % wavelet transform
%             imagesc(timeVector,1:10,abs(ccfs));
%             colormap jet; axis xy;
%             xlabel('time (ms)'); ylabel('Scales');
%             title('Wavelet Transform')
            
           %pause
           wTrials(:,:,stimChunkNo) = abs(ccfs);
           eval(['wTrials' num2str(stimToChunk) ' = wTrials;']);
        end
    end
end

% %
%R-squared for wavelet analysis

% OfflineAnalysisSpectral script
% Make sure BCI2000\Tools /s are in the Matlab path

epochLength = 128; % number of samples
stimuli = states.StimulusCode;

for types = 1:6
eval(['WTrials' '=' 'wTrials' num2str(types) ';'])
% StimulusCodeChangeIndices
stimToChunk = types;
stimEpoch = find(stimuli==stimToChunk);
% stimulus begin index
stimBeginInd = [1; stimEpoch(find(diff(double(stimEpoch))~= 1) + 1)];
% Remove 1st chunk
if stimuli(1)==stimToChunk
    % Sometimes, there is an incorrectly recorded, short burst of stimuli 1 at the
    % beginning of the data set for no reason. Remove that.
    stimBeginInd = stimBeginInd(2:end);
end

tempMatrix = zeros(epochLength, 10, 40); %time vs. scales vs. cwt values
for k = 1:length(stimBeginInd)
   data = WTrials(:,:,k);
   tempMatrix(:,:,k) = data'; 
end

eval(['RTrials' num2str(types) ' = tempMatrix;']);
%stimEpoch = [];
tempMatrix = [];
stimBeginInd = [];
end

NoTargetTrials = cat(3, RTrials1, RTrials2, RTrials3, RTrials4, RTrials6, RTrials7, RTrials8);
TargetTrials = cat(3,RTrials9,RTrials10);

% R_Squared = calc_rsqu(RestTrials,GreenTrials,1);
% R_Squared2 = calc_rsqu(RestTrials,RedTrials,1);
R_Squared3 = calc_rsqu(NoTargetTrials,TargetTrials, 1);
timeVector = (1:epochLength)/128*1000;
%channelVector = 1:14;
%figure(1),surf(timeVector, channelVector, R_Squared3','EdgeColor','none'),view(2),colorbar;
figure(),imagesc(timeVector, 1:10, R_Squared3'), colorbar
set(gcf,'Renderer','Zbuffer');
title('R^2 Plot'),xlabel('Time(ms)'),ylabel('Scale')

%save('OfflineAnalysisSpectral');
