%

%% Load file
filepath = 'C:\BCI2000\data\samplefiles'; filename = 'eeg2_1.dat';
[ signal, states, parameters ] = load_bcidat([filepath '\' filename]);
signal = double(signal)/1000;

% % Parameter extraction
sampleRate = parameters.SamplingRate.NumericValue;
stimuli = states.StimulusCode;
%% Perform a CAR filter
% This might be bad for ERPs...
for ch=1:size(signal, 2)
    signal(:, ch)=signal(:, ch)-mean(signal(:, ch));
end

signal = carFilt(signal,2);

% % Group data by stimulus code
% StimulusCodeChangeIndices
% These are the indices where the stimulus code changes from one value
% to another. "diff" takes a derivative. We need to add some more to
% prevent allocating memory with each iteration.
SCCI = [1; find(diff(double(states.StimulusCode))~=0)+1; length(states.StimulusCode)];
SCCI = SCCI(1:30);

timeIntervalSize = 128; %64 samples is 500 ms
%% 
NullIndex = 1;
OddballIndex = 1;

for k = 1:length(SCCI)-1
    disp(k)
    % Signal analyzed in this particular epoch
    SignalTrial = signal((SCCI(k):SCCI(k)+timeIntervalSize-1),:);
    
    
    for electrode = 1:size(signal,2)
          PositiveSignalTrial = SignalTrial(:,electrode);
%         PositiveSignalTrial = SignalTrial(:,electrode).*SignalTrial(:,electrode);
          plot(PositiveSignalTrial); title(num2str(states.StimulusCode(SCCI(k)))); pause

        % Assign the average power to the corresponding stimulus
        if (states.StimulusCode(SCCI(k)) == 1)
            NullTrials(:,electrode,NullIndex) = PositiveSignalTrial;
            NullIndex = NullIndex+1;
            
            plot(NullTrials(:,electrode,NullIndex)); hold on
            plot(PositiveSignalTrial); hold off
            title([num2str(NullIndex) ', ' num2str(electrode)]); pause
           
        end
        
        if (states.StimulusCode(SCCI(k)) == 0)
            OddballTrials(:,electrode,OddballIndex) = PositiveSignalTrial;
            OddballIndex = OddballIndex+1;
            
            plot(OddballTrials(:,electrode,NullIndex)); hold on
            plot(PositiveSignalTrial); hold off
            title([num2str(OddballIndex) ', ' num2str(electrode)]); pause
        end
    end
end
% % 
% Calculated R^2
    % Requires calc_rsqu.m and rsqu.m
R_Squared = calc_rsqu(double(NullTrials), double(OddballTrials),1);

% % Plot
timeVector = (1:timeIntervalSize)/128*1000;
channelVector = 1:64;
figure(1),surf(timeVector, channelVector, R_Squared','EdgeColor','none'),view(2),colorbar;
set(gcf,'Renderer','Zbuffer');

title('R^2 Plot'),xlabel('Time (ms)'),ylabel('Channel')