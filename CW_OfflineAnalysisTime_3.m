% OfflineAnalysisSpectral script
% Make sure BCI2000\Tools /s are in the Matlab path

% Load .dat files
% [Filenames, BCIDatPath] = uigetfile( {'*.dat','BCI 2000 Mat Files (*.dat)'},'MultiSelect', 'on' ) ;
% Filenames = strcat(BCIDatPath,Filenames) ;
% files = struct( 'name', Filenames) ;
clear all
clc
[ signal, states, parameters ] = load_bcidat('C:\BCI2000\data\ChristineWeston001\ChristineWestonS001R03.dat');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%signal: NxM array of samples from the electrodes during the experiment. N
%is the number of samples and M is the number of electrodes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%parameters.SamplingRate.NumericValue: Sample rate for data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%states.StimulusBegin: Nx1 column vector where N is the number of samples.
%This array alternates between 0 and 1 and changes everytime 
%states.StimulusCode changes indicating a new condition has begun.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%states.StimulusCode: Nx1 column vector where N is the number of samples.
%This array indicates the condition number for every sample.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%For example, in data\samplefiles\eeg1_1.dat the subjects starts at rest 
%(condition=0) and then raises both feet (condition2) at sample 673 and
%then goes back to rest at sample 1328. Therefore:
%states.SimulusBegin(1  :672 )=0 states.SimulusCode(1  :672 )=0   Rest
%states.SimulusBegin(673:1328)=1 states.SimulusCode(673:1328)=2   Both Feet
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Power Spectrum Constants
FSampling=parameters.SamplingRate.NumericValue; %sampling rate

for ch=1:size(signal, 2) %subtracts the offset
    signal(:, ch)=signal(:, ch)-mean(signal(:, ch));
end

signal = carFilt(signal,2); %filters the signal

epochLength = 128; % number of samples
stimuli = states.StimulusCode;

for types = 1:6
    
% StimulusCodeChangeIndices
stimToChunk = types;
stimEpoch = find(stimuli==stimToChunk);
% stimulus begin index
stimBeginInd = [1; stimEpoch(find(diff(double(stimEpoch))~= 1) + 1)];
% Remove 1st chunk
if stimuli(1)==stimToChunk
    % Sometimes, there is an incorrectly recorded, short burst of stimuli 1 at the
    % beginning of the data set for no reason. Remove that.
    stimBeginInd = stimBeginInd(2:end);
end

electrodes = size(signal,2);
tempMatrix = zeros(epochLength, electrodes, length(stimBeginInd)); 
for k = 1:length(stimBeginInd)
   data = signal((stimBeginInd(k):stimBeginInd(k)+epochLength-1),:);
   tempMatrix(:,:,k) = data; 
end

eval(['Trials' num2str(types) ' = tempMatrix;']);
%stimEpoch = [];
tempMatrix = [];
stimBeginInd = [];
end

NoTargetTrials = cat(3, Trials5, Trials2, Trials3, Trials4, Trials6);
TargetTrials = Trials1;

% R_Squared = calc_rsqu(RestTrials,GreenTrials,1);
% R_Squared2 = calc_rsqu(RestTrials,RedTrials,1);
R_Squared3 = calc_rsqu(NoTargetTrials,TargetTrials, 1);
timeVector = (1:epochLength)/128*1000;
channelVector = 1:14;
figure(1),surf(timeVector, channelVector, R_Squared3','EdgeColor','none'),view(2),colorbar;
set(gcf,'Renderer','Zbuffer');
title('R^2 Plot'),xlabel('Time(ms)'),ylabel('Channel')

%save('OfflineAnalysisSpectral');



