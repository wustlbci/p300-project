% 12/13/13
% Plot components of ICA
% Jason Dunkley and Jenny Liu
% After performing independent componenent analysis via EEGLab,the original
% EEG data and ICA weights can be used to calculate the components and 
% plot them.


% datacomp is matrix of components (ICA outputs)
d = ALLEEG.data;
w = ALLEEG.icaweights;
dataComp = w * d; 

%%
% Plotting
dataComp = dataComp';
% determine range of EEG samples you would like to plot
%   For example, 1:128 is the first second (128 Hz digitization)
sampleTime = 1:(5*128); % first 5 seconds
plot(dataComp(sampleTime,:)) % plots all 14 channels currently
legend('1','2','3','4','5','6','7','8','9','10','11','12','13','14')
xlabel('sample points')
ylabel('Voltage (uV)')
title('Components of EEG Data')



