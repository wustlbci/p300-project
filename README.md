P300 Project
============

The P300 project is our attempt to use humans as hardware to sort images using 
the P300.  After priming the user with the object to identify (e.g. cat), we 
should be able to flash a bunch of images at the person, whose brain waves 
upon seeing "cat" will be different than when viewing the other images.  Our 
goal is to be able to recreate this phenomena on the Emotiv and then try to 
answer some "research" questions, such as, how long does the effect last? or 
anything y'all can think of.  Our advisers are Dr. Nehorai and Elad Gilboa in 
the electrical engineering department.

Goals
-----
We currently have 3 aims:
-1. Show that we can get P300 data (quick and dirty)
-2. Show that we can separate images based on "interest" (the Pohlmeyer paper)
-3. Use our system visual identification, preferably teaming up with an 
existing computer-vision project with working image detection algorithms. 
Maybe Robert Pless or Killian Weinberger at Wash U would be interested.

Ideas
-----
Thoughts about applications:
-1. Assist machine-based image sorting
-2. See if training improves efficacy
-3. Send the subject outside to identify things they see, maybe in an 
attention-game, such as the gorilla/ball-tossing scenario or a mock crime scene.

Project Structure
-----------------
- Prototype Code: (insert summary)      TODO
- Wavelet Stats: (insert summary)       TODO


TODO
----
- README's for subdirectories.


